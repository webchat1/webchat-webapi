package com.example.web.service;

import com.example.web.entity.UserEntity;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;


@Service
public class TestMq {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public String test() {
        UserEntity userEntity = new UserEntity();
        double v = Math.random() * 10;
        String s = "哈哈哈" + v;
        userEntity.setName(s);
        rabbitTemplate.convertAndSend("exchange3", "user.#", userEntity);
        return s;
    }

    public String test1() {
        HashMap<Object, Object> objectObjectHashMap = new HashMap<>();
        double v = Math.random() * 10;
        String s = "哈哈哈" + v;
        objectObjectHashMap.put("test1", v);
        rabbitTemplate.convertAndSend("exchange4", "user.#", objectObjectHashMap);
        return s;
    }
}
