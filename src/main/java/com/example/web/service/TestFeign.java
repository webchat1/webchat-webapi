package com.example.web.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "minio-service")
public interface TestFeign {
    @GetMapping("/test")
    String test(@RequestParam(value = "a") String b);
}
