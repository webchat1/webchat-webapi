package com.example.web.controller;

import com.example.web.service.TestMq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
public class TestController {
    @Autowired
    private TestMq testMq;

    @RequestMapping("/test")
    public String test(){
        return testMq.test();
    }
    @RequestMapping("/test1")
    public String test1(){
        return testMq.test1();
    }
}
